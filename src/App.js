import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import logo from './logo.svg';
import './App.css';
import Status from './Status';


class App extends Component {
  render() {
    const style = {
			display: "flex",
			justifyContent: "space-around",
			paddingTop: "20px"
    };
    
    const listOne = [
			{ id: 1, title: "Task 1", description: 'This is the task number 1', status: "Development" },
			{ id: 2, title: "Task 2", description: 'This is the task number 2', status: "Development" },
			{ id: 3, title: "Task 3", description: 'This is the task number 3', status: "Development" }
		];
 
		const listTwo = [
			{ id: 4, title: "Task 4", description: 'This is the task number 4', status: "Design" },
			{ id: 5, title: "Task 5", description: 'This is the task number 5', status: "Design" },
			{ id: 6, title: "Task 6", description: 'This is the task number 6', status: "Design" }
		];
 
		const listThree = [
			{ id: 7, title: "Task 7", description: 'This is the task number 7', status: "Backlog" },
			{ id: 8, title: "Task 8", description: 'This is the task number 8', status: "Backlog" },
			{ id: 9, title: "Task 9", description: 'This is the task number 9', status: "Backlog" }
		];
    return (
      <MuiThemeProvider>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Kanban Board</h1>
          <h5 className="">by Eric Nordelo Galiano</h5>
        </header>
        <p className="App-intro">
          <div style={{...style}}>
            <Status id={'Development'} list={listOne} />
            <Status id={'Design'} list={listTwo} />
            <Status id={'Backlog'} list={listThree} />
          </div>
        </p>
      </div>
      </MuiThemeProvider>
    );
  }
}


export default DragDropContext(HTML5Backend)(App);