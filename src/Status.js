import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import update from 'react-addons-update';
import Task from './Task';

class Status extends Component {

    constructor(props) {
        super(props);		
		this.state = { tasks: props.list };
    }

    pushTask(task) {
		const newTask = update(task, {
			status: {
				$set: this.props.id
			}
		});
        this.setState(update(this.state, {
            tasks: {
                $push: [ newTask ]
            }
        }));
    }

    removeTask(index) {		
        this.setState(update(this.state, {
            tasks: {
                $splice: [
                    [index, 1]
                ]
            }
        }));
    }

    moveTask(dragIndex, hoverIndex) {
        const { tasks } = this.state;		
        const dragTask = tasks[dragIndex];

        this.setState(update(this.state, {
            tasks: {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragTask]
                ]
            }
        }));
    }

    render() {
		const { tasks } = this.state;
		
		const { canDrop, isOver, connectDropTarget } = this.props;
		const isActive = canDrop && isOver;
		const style = {
			width: "100%",
			border: '1px dashed gray'
		};
 
		const backgroundColor = isActive ? 'lightgreen' : '#FFF';

		return connectDropTarget(
			<div style={{...style, backgroundColor}}>
				{tasks.map((task, i) => {
					return (
						<Task 
							key={task.id}
							index={i}
							listId={this.props.id}
							task={task}														
							removeTask={this.removeTask.bind(this)}
							moveTask={this.moveTask.bind(this)} />
					);
				})}
			</div>
		);
    }
}

const taskTarget = {
	drop(props, monitor, component ) {
		const { id } = props;
		const sourceObj = monitor.getItem();		
		if ( id !== sourceObj.listId ) component.pushTask(sourceObj.task);
		return {
			listId: id
		};
	}
}
 
export default DropTarget("TASK", taskTarget, (connect, monitor) => ({
	connectDropTarget: connect.dropTarget(),
	isOver: monitor.isOver(),
	canDrop: monitor.canDrop()
}))(Status);